use adw::prelude::*;
use gtk::{glib, subclass::prelude::*};
use zbus::zvariant;

use crate::message::Message;

mod imp {
    use std::cell::RefCell;

    use super::*;
    use crate::color_widget::ColorWidget;

    #[derive(Debug, Default, glib::Properties, gtk::CompositeTemplate)]
    #[properties(wrapper_type = super::DetailsView)]
    #[template(resource = "/org/freedesktop/Bustle/ui/details_view.ui")]
    pub struct DetailsView {
        #[property(get, set = Self::set_message, explicit_notify, nullable)]
        pub(super) message: RefCell<Option<Message>>,

        #[template_child]
        pub(super) scrolled_window: TemplateChild<gtk::ScrolledWindow>,
        #[template_child]
        pub(super) type_row: TemplateChild<adw::ActionRow>,
        #[template_child]
        pub(super) sender_row: TemplateChild<adw::ActionRow>,
        #[template_child]
        pub(super) destination_row: TemplateChild<adw::ActionRow>,
        #[template_child]
        pub(super) component_row: TemplateChild<adw::ActionRow>,
        #[template_child]
        pub(super) component_color: TemplateChild<ColorWidget>,
        #[template_child]
        pub(super) path_row: TemplateChild<adw::ActionRow>,
        #[template_child]
        pub(super) member_row: TemplateChild<adw::ActionRow>,
        #[template_child]
        pub(super) interface_row: TemplateChild<adw::ActionRow>,
        #[template_child]
        pub(super) error_row: TemplateChild<adw::ActionRow>,
        #[template_child]
        pub(super) flags_row: TemplateChild<adw::ActionRow>,
        #[template_child]
        pub(super) size_row: TemplateChild<adw::ActionRow>,
        #[template_child]
        pub(super) signature_row: TemplateChild<adw::ActionRow>,
        #[template_child]
        pub(super) response_time_row: TemplateChild<adw::ActionRow>,
        #[template_child]
        pub(super) arguments_text_buffer: TemplateChild<gtk::TextBuffer>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for DetailsView {
        const NAME: &'static str = "BustleDetailsView";
        type Type = super::DetailsView;
        type ParentType = gtk::Widget;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for DetailsView {
        fn dispose(&self) {
            self.dispose_template();
        }
    }

    impl WidgetImpl for DetailsView {}

    impl DetailsView {
        fn set_message(&self, message: Option<Message>) {
            if message == self.message.replace(message.clone()) {
                return;
            }

            let header = message.as_ref().map(|message| message.header());

            self.message.replace(message.clone());
            self.type_row.set_subtitle(
                &message
                    .as_ref()
                    .map(|m| m.message_type().i18n())
                    .unwrap_or_default(),
            );
            self.sender_row.set_subtitle(
                &message
                    .as_ref()
                    .map(|m| m.sender_display())
                    .unwrap_or_default(),
            );
            self.destination_row.set_subtitle(
                &message
                    .as_ref()
                    .map(|m| m.destination_display())
                    .unwrap_or_default(),
            );
            let message_tag = message
                .as_ref()
                .map(|m| m.message_tag())
                .unwrap_or_default();
            self.component_color.set_rgba(message_tag.color());
            self.component_row.set_subtitle(&message_tag.name());
            self.path_row.set_subtitle(
                &message
                    .as_ref()
                    .map(|m| m.path_display())
                    .unwrap_or_default(),
            );
            self.interface_row.set_subtitle(
                &message
                    .as_ref()
                    .map(|m| m.interface_display())
                    .unwrap_or_default(),
            );
            self.member_row.set_subtitle(
                &message
                    .as_ref()
                    .map(|m| m.member_display())
                    .unwrap_or_default(),
            );

            self.error_row.set_visible(
                message
                    .as_ref()
                    .is_some_and(|m| m.message_type().is_error()),
            );
            self.error_row.set_subtitle(
                &header
                    .as_ref()
                    .and_then(|m| m.error_name().map(|e| e.to_string()))
                    .unwrap_or_default(),
            );

            let flags = message
                .as_ref()
                .map(|i| i.flags_display())
                .unwrap_or_default();
            self.flags_row.set_visible(!flags.is_empty());
            self.flags_row.set_subtitle(&flags);

            self.size_row.set_subtitle(
                &header
                    .as_ref()
                    .map(|h| glib::format_size(h.primary().body_len() as u64))
                    .unwrap_or_default(),
            );
            let signature = header
                .as_ref()
                .and_then(|h| h.signature())
                .map(|s| s.to_string())
                .unwrap_or_default();
            self.signature_row.set_visible(!signature.is_empty());
            self.signature_row.set_subtitle(&signature);

            let response_time = message.as_ref().and_then(|m| m.response_time());
            self.response_time_row.set_visible(
                response_time.is_some()
                    && message
                        .as_ref()
                        .map_or(false, |m| m.message_type().is_method_return()),
            );
            self.response_time_row.set_subtitle(
                &response_time
                    .map(|ts| format!("{:.2} ms", ts.as_millis_f64()))
                    .unwrap_or_default(),
            );

            self.arguments_text_buffer.set_text(
                &message
                    .as_ref()
                    .and_then(|m| {
                        m.body()
                            .deserialize::<zvariant::Structure<'_>>()
                            .ok()
                            .map(|s| s.to_string())
                    })
                    .unwrap_or_default(),
            );

            self.obj().notify_message();
        }
    }
}

glib::wrapper! {
     pub struct DetailsView(ObjectSubclass<imp::DetailsView>)
        @extends gtk::Widget;
}
